package ru.t1.rleonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm.project (id, name, description, status, created, user_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{created}, #{userId});")
    void add(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm.project WHERE id = #{id} AND user_id = #{userId};")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm.project WHERE user_id = #{userId};")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("TRUNCATE TABLE tm.project;")
    void clearAll();

    @Select("SELECT * FROM tm.project;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull List<ProjectDTO> findAllProjects();

    @Select("SELECT * FROM tm.project WHERE user_id = #{userId} ORDER BY #{sort};")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull List<ProjectDTO> findAll(@NotNull @Param("userId") String userId, @NotNull @Param("sort") String sort);

    @Select("SELECT * FROM tm.project WHERE id = #{id} AND user_id = #{userId} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    ProjectDTO findOneById(@NotNull @Param("userId") String userId, @Param("id") @NotNull String id);

    @Update("UPDATE tm.project SET status = #{status} WHERE id = #{id} AND user_id = #{userId};")
    void changeStatusById(@Nullable @Param("userId") String userId,
                          @Nullable @Param("id") String id,
                          @Nullable @Param("status") Status status);

    @Update("UPDATE tm.project SET name = #{name}, description = #{description} " +
            "WHERE id = #{id} AND user_id = #{userId};")
    void updateById(@Nullable @Param("userId") String userId,
                    @Nullable @Param("id") String id,
                    @Nullable @Param("name") String name,
                    @Nullable @Param("description") String description);

}
