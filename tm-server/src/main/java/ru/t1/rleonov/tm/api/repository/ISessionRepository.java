package ru.t1.rleonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.SessionDTO;

public interface ISessionRepository {

    @Insert("INSERT INTO tm.session (id, name, description, status, created, role, user_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{created}, #{role}, #{userId});")
    void add(@NotNull SessionDTO session);

    @Delete("DELETE FROM tm.session WHERE id = #{id};")
    void removeById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm.session WHERE id = #{id} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    SessionDTO findOneById(@NotNull @Param("id") String id);

}
