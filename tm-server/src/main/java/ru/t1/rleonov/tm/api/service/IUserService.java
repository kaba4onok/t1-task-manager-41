package ru.t1.rleonov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    @SneakyThrows
    List<UserDTO> findAll();

    @Nullable
    @SneakyThrows
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    @SneakyThrows
    UserDTO lockUserByLogin(@Nullable String login);

    @SneakyThrows
    UserDTO unlockUserByLogin(@Nullable String login);

    @SneakyThrows
    void set(@NotNull Collection<UserDTO> users);

    @SneakyThrows
    void clear();

}
