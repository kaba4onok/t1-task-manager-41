package ru.t1.rleonov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    String getSortType(@Nullable Sort sort);

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    @SneakyThrows
    List<TaskDTO> findAll();

    @Nullable
    TaskDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void set(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

}
