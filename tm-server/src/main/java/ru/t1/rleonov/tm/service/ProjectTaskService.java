package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.rleonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    public IProjectService projectService;

    @NotNull
    public ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.bindTaskToProject(projectId, taskId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.unbindTaskFromProject(taskId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final TaskDTO task : tasks) taskService.removeById(userId, task.getId());
        projectService.removeById(userId, projectId);
        return project;
    }

}
