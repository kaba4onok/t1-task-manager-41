package ru.t1.rleonov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.SessionDTO;

public interface ISessionService {

    @NotNull
    @SneakyThrows
    SessionDTO create(@Nullable SessionDTO session);

    Boolean existsById(@Nullable String id);

    @Nullable
    @SneakyThrows
    SessionDTO findOneById(@Nullable String id);

    @NotNull
    @SneakyThrows
    SessionDTO removeById(@Nullable String id);

}
